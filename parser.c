#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>

#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>

#include <math.h>

#include "ubx_defines.h"

struct posit posit;
uint8_t *positRaw = (uint8_t *)&posit;

struct runningStdDev {
  double M, S;
  unsigned long cnt;
} stdlat, stdlon, stdhMSL;

#define INIT_STDDEV(x) std ## x.M = 0.0; std ## x.S = 0.0; std ## x.cnt = 1;
#define ADD_STDDEV(x, v) { \
double tmpM = std ## x.M; \
std ## x.M += ((v)-tmpM) / (std ## x.cnt); \
std ## x.S += ((v)-tmpM) * ((v) - std ## x.M); \
++std ## x.cnt; \
}
#define GET_STDDEV(x) sqrt(std ## x.S / (std ## x.cnt-2.0))

int decode(uint8_t c) {
  static uint8_t len = 0;
  static enum { S_SYNC, S_SYNC2, S_PAYLOAD } state = S_SYNC;
  int retval = 0;
  switch(state) {
    case S_SYNC:
    if(c == 0xB5)
      state = S_SYNC2;
    else
      printf("Sync loss\n");
    break;
    case S_SYNC2:
    if(c == 0x62)
      state = S_PAYLOAD;
    else if(c == 0xB5)
      state = S_SYNC2;
    else
      state = S_SYNC;
    len = 0;
    break;
    case S_PAYLOAD:
    positRaw[len++] = c;
    if(len == sizeof(posit)) {
      retval = 1;
      state = S_SYNC;
    }
    break;
    default:
    state = S_SYNC;
    break;
  }
  return retval;
}

double earthRadius(double lat) {
  double a = 6378137.0;
  double b = 6356752.3;
  double c = cos(lat);
  double s = sin(lat);
  double t1 = a * a * c;
  double t2 = b * b * s;
  double t3 = a * c;
  double t4 = b * s;
  return sqrt((t1*t1 + t2*t2) / (t3*t3 + t4*t4));
}

double toECEF(double lat, double lon, double height,
  double *x, double *y, double *z) {
  lat = lat * M_PI / 180.0;
  lon = lon * M_PI / 180.0;
  height = height + earthRadius(lat);

  double cosLon = cos(lon);
  double sinLon = sin(lon);
  double cosLat = cos(lat);
  double sinLat = sin(lat);

  *x = cosLon * cosLat * height;
  *y = sinLon * cosLat * height;
  *z = sinLat * height;
}

double distanceBetween(double lat, double lon, double height, double lat2, double lon2, double height2) {
  double x, y, z, lx, ly, lz;

  toECEF(lat, lon, height, &x, &y, &z);
  toECEF(lat2, lon2, height2, &lx, &ly, &lz);
  double dx = x - lx;
  double dy = y - ly;
  double dz = z - lz;

  double distance = sqrt(dx*dx + dy*dy + dz*dz);

  return distance;
}

int main(int argc, char **argv) {
  int fd, len;
  FILE *kml;
  char *coordinatesBuf;
  unsigned long coordSize = 65536;
  unsigned long coordPos = 0;
  char buffer[1024];
  if(argc == 1) {
    printf("Need a filename");
    exit(1);
  }
  printf("sizeof(posit): %lu\n", sizeof(posit));
  fd = open(argv[1], O_RDONLY);
  read(fd, buffer, 6);

  xmlDocPtr doc = NULL;
  xmlNodePtr root_node = NULL, document_node = NULL, style_node = NULL,
  coordinates = NULL, gx_track = NULL;
  xmlNodePtr temp_node = NULL;

  doc = xmlNewDoc("1.0");
  root_node = xmlNewNode(NULL, "kml");
  xmlNewProp(root_node, "xmlns", "http://www.opengis.net/kml/2.2");
  xmlNewProp(root_node, "xmlns:gx", "http://www.google.com/kml/ext/2.2");
  xmlDocSetRootElement(doc, root_node);

  coordinatesBuf = (char *)malloc(coordSize);

  document_node = xmlNewChild(root_node, NULL, "Document", NULL);

  xmlNewChild(document_node, NULL, "name", argv[1]);

  style_node = xmlNewChild(document_node, NULL, "Style", NULL);
  xmlNewProp(style_node, "id", "trackStyle");

  temp_node = xmlNewChild(style_node, NULL, "LineStyle", NULL);
  xmlNewChild(temp_node, NULL, "color", "ff0000f0");
  xmlNewChild(temp_node, NULL, "width", "4");

  style_node = xmlNewChild(document_node, NULL, "Style", NULL);
  xmlNewProp(style_node, "id", "pathStyle");

  temp_node = xmlNewChild(style_node, NULL, "LineStyle", NULL);
  xmlNewChild(temp_node, NULL, "color", "f8000080");
  xmlNewChild(temp_node, NULL, "width", "8");

  char nbuf[64];
  snprintf(nbuf, 64, "Coords %d", 1);
  style_node = xmlNewChild(document_node, NULL, "Placemark", NULL);
  xmlNewChild(style_node, NULL, "name", nbuf);
  xmlNewChild(style_node, NULL, "styleUrl", "#pathStyle");

  temp_node = xmlNewChild(style_node, NULL, "LineString", NULL);
  xmlNewChild(temp_node, NULL, "extrude", "1");
  coordinates = xmlNewChild(temp_node, NULL, "coordinates", NULL);

  // GX Coords format
  style_node = xmlNewChild(document_node, NULL, "Placemark", NULL);
  xmlNewChild(style_node, NULL, "name", "GX Coords 1");
  xmlNewChild(style_node, NULL, "styleUrl", "#trackStyle");

  temp_node = xmlNewChild(style_node, NULL, "gx:MultiTrack", NULL);
  xmlNewChild(temp_node, NULL, "altitudeMode", "absolute");
  gx_track = xmlNewChild(temp_node, NULL, "gx:Track", NULL);
  xmlNewChild(gx_track, NULL, "altitudeMode", "absolute");

  double lat = 0.0, lon = 0.0, hMSL = 0.0;
  double flat = 0.0, flon = 0.0, fhMSL = 0.0;
  double minhMSL = nan(""), maxhMSL = 0.0;
  int cnt = 0;

  INIT_STDDEV(lat)
  INIT_STDDEV(lon)
  INIT_STDDEV(hMSL)
  double llat = 0.0, llon = 0.0, lhMSL = 0.0;
  while((len = read(fd, buffer, 1024)) > 0) {
    int i;
    for(i = 0; i < len; i++) {
      if(decode(buffer[i])) {
        lat = (posit.lat/1e7 + (double)cnt*lat)/(cnt+1.0);
        lon = (posit.lon/1e7 + (double)cnt*lon)/(cnt+1.0);
        hMSL = (posit.hMSL/1e3 + (double)cnt*hMSL)/(cnt+1.0);
        flat += posit.lat/1e7; flon += posit.lon/1e7; fhMSL += posit.hMSL/1e3;
        ++cnt;
        ADD_STDDEV(lat, posit.lat/1e7)
        ADD_STDDEV(lon, posit.lon/1e7)
        ADD_STDDEV(hMSL, posit.hMSL/1e3)
        minhMSL = fmin(minhMSL, posit.hMSL/1e3);
        maxhMSL = fmax(maxhMSL, posit.hMSL/1e3);
        printf("%04u-%02u-%02u %02u:%02u:%02.2f %0.07f %0.07f %0.2fm %0.07f %0.07f %0.2fm %0.2fkm/h %0.1f\n",
         posit.year, posit.month, posit.day,
         posit.hour, posit.min, (posit.sec+(posit.nano/1e9)),
         posit.lat/1e7, posit.lon/1e7,
         posit.hMSL/1e3,
         lat, lon,
         hMSL,
         (posit.gSpeed/1e3)*3.6,
         posit.heading/1e5);
        char buf[128];
        if(cnt == 1) {
          temp_node = xmlNewChild(document_node, NULL, "Placemark", NULL);
          xmlNewChild(temp_node, NULL, "name", "Start");
          temp_node = xmlNewChild(temp_node, NULL, "Point", NULL);
          snprintf(buf, 128, "%0.6f,%0.6f,%0.1f", posit.lon/1e7, posit.lat/1e7, posit.hMSL/1e3);
          xmlNewChild(temp_node, NULL, "coordinates", buf);
        }
        if(cnt % 50000 == 0) {
          xmlNodeSetContent(coordinates, coordinatesBuf);
          style_node = xmlNewChild(document_node, NULL, "Placemark", NULL);
          snprintf(nbuf, 64, "Coords %d", (cnt/50000)+1);
          xmlNewChild(style_node, NULL, "name", nbuf);
          xmlNewChild(style_node, NULL, "styleUrl", "#pathStyle");

          temp_node = xmlNewChild(style_node, NULL, "LineString", NULL);
          xmlNewChild(temp_node, NULL, "extrude", "1");
          coordinates = xmlNewChild(temp_node, NULL, "coordinates", NULL);

          // GX Coords format
          style_node = xmlNewChild(document_node, NULL, "Placemark", NULL);
          snprintf(nbuf, 64, "GX Coords %d", (cnt/50000)+1);
          xmlNewChild(style_node, NULL, "name", nbuf);
          xmlNewChild(style_node, NULL, "styleUrl", "#trackStyle");

          temp_node = xmlNewChild(style_node, NULL, "gx:MultiTrack", NULL);
          xmlNewChild(temp_node, NULL, "altitudeMode", "absolute");
          gx_track = xmlNewChild(temp_node, NULL, "gx:Track", NULL);
          xmlNewChild(gx_track, NULL, "altitudeMode", "absolute");
          coordPos = 0;
        }
        double dfl = distanceBetween(posit.lat/1e7, posit.lon/1e7, posit.hMSL/1e3,
          llat, llon, lhMSL);
        if(dfl > 1.0) {
          printf("Distance: %f\n", dfl);
          llat = posit.lat/1e7;
          llon = posit.lon/1e7;
          lhMSL = posit.hMSL/1e3;
          int r = snprintf(coordinatesBuf + coordPos, 128, "%0.6f,%0.6f,%0.1f\n", posit.lon/1e7, posit.lat/1e7, posit.hMSL/1e3);
          coordPos += r;
          if(coordSize - coordPos < 128) {
            coordSize *= 2;
            coordinatesBuf = realloc(coordinatesBuf, coordSize);
          }
          struct tm t;
          t.tm_sec = posit.sec;
          t.tm_min = posit.min;
          t.tm_hour = posit.hour;
          t.tm_mday = posit.day;
          t.tm_mon = posit.month-1;
          t.tm_year = posit.year-1900;
          strftime(buf, 128, "%Y-%m-%dT%H:%M:", &t);
          snprintf(buf + strlen(buf), 64, "%02.03fZ", posit.sec+(posit.nano/1e9));
          xmlNewChild(gx_track, NULL, "when", buf);
          snprintf(buf, 128, "%0.6f %0.6f %0.1f", posit.lon/1e7, posit.lat/1e7, posit.hMSL/1e3);
          xmlNewChild(gx_track, NULL, "gx:coord", buf);
        }
      }
    }
  }
  xmlNodeSetContent(coordinates, coordinatesBuf);
  temp_node = xmlNewChild(document_node, NULL, "Placemark", NULL);
  xmlNewChild(temp_node, NULL, "name", "End");
  temp_node = xmlNewChild(temp_node, NULL, "Point", NULL);
  char buf[128];
  snprintf(buf, 128, "%0.6f,%0.6f,%0.1f", posit.lon/1e7, posit.lat/1e7, posit.hMSL/1e3);
  xmlNewChild(temp_node, NULL, "coordinates", buf);
  xmlSaveFormatFileEnc("output.kml", doc, "UTF-8", 1);
  xmlFreeDoc(doc);
  xmlCleanupParser();
  close(fd);
  printf("%0.07f %0.07f %0.2fm %0.07fm stddev (%0.02fm %0.02fm)\n", flat/cnt, flon/cnt, fhMSL/cnt, GET_STDDEV(hMSL), minhMSL, maxhMSL);
  printf("%f %f %f\n", GET_STDDEV(lat), stdlat.M, stdlat.S);
  printf("%f %f %f\n", GET_STDDEV(lon), stdlon.M, stdlon.S);
  printf("%f %f %f\n", GET_STDDEV(hMSL), stdhMSL.M, stdhMSL.S);
}
