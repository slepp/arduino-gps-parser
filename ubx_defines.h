#define UBX_NAV 0x01
#define UBX_RXM 0x02
#define UBX_INF 0x04
#define UBX_ACK 0x05
#define UBX_CFG 0x06
#define UBX_MON 0x0A
#define UBX_AID 0x0B
#define UBX_TIM 0x0D
#define UBX_LOG 0x21

#define UBX_ACK_ACK 0x01
#define UBX_ACK_NAK 0x00

#define UBX_AID_ALM 0x30
#define UBX_AID_ALPSRV 0x32
#define UBX_AID_ALP 0x50
#define UBX_AID_AOP 0x33
#define UBX_AID_DATA 0x10
#define UBX_AID_EPH 0x31
#define UBX_AID_HUI 0x02
#define UBX_AID_INI 0x01
#define UBX_AID_REQ 0x00

#define UBX_CFG_ANT 0x13
#define UBX_CFG_CFG 0x09
#define UBX_CFG_DAT 0x06
#define UBX_CFG_GNSS 0x3E
#define UBX_CFG_INF 0x02
#define UBX_CFG_ITFM 0x39
#define UBX_CFG_LOGFILTER 0x47
#define UBX_CFG_MSG 0x01
#define UBX_CFG_NAV5 0x24
#define UBX_CFG_NAVX5 0x23
#define UBX_CFG_NMEA 0x17
#define UBX_CFG_NVS 0x22
#define UBX_CFG_PM2 0x3B
#define UBX_CFG_PRT 0x00
#define UBX_CFG_RATE 0x08
#define UBX_CFG_RINV 0x34
#define UBX_CFG_RST 0x04
#define UBX_CFG_RXM 0x11
#define UBX_CFG_SBAS 0x16
#define UBX_CFG_TP5 0x31
#define UBX_CFG_USB 0x1B

#define UBX_INF_DEBUG 0x04
#define UBX_INF_ERROR 0x00
#define UBX_INF_NOTICE 0x02
#define UBX_INF_TEST 0x03
#define UBX_INF_WARNING 0x01

#define UBX_LOG_CREATE 0x07
#define UBX_LOG_ERASE 0x03
#define UBX_LOG_FINDTIME 0x0E
#define UBX_LOG_INFO 0x08
#define UBX_LOG_RETRIEVEPOS 0x0b
#define UBX_LOG_RETRIEVESTRING 0x0d
#define UBX_LOG_RETRIEVE 0x09
#define UBX_LOG_STRING 0x04

#define UBX_MON_HW2 0x0B
#define UBX_MON_HW 0x09
#define UBX_MON_IO 0x02
#define UBX_MON_MSGPP 0x06
#define UBX_MON_RXBUF 0x07
#define UBX_MON_RXR 0x21
#define UBX_MON_TXBUF 0x08
#define UBX_MON_VER 0x04

#define UBX_NAV_AOPSTATUS 0x60
#define UBX_NAV_CLOCK 0x22
#define UBX_NAV_DGPS 0x31
#define UBX_NAV_DOP 0x04
#define UBX_NAV_POSECEF 0x01
#define UBX_NAV_POSLLH 0x02
#define UBX_NAV_PVT 0x07
#define UBX_NAV_SBAS 0x32
#define UBX_NAV_SOL 0x06
#define UBX_NAV_STATUS 0x03
#define UBX_NAV_SVINFO 0x30
#define UBX_NAV_TIMEGPS 0x20
#define UBX_NAV_TIMEUTC 0x21
#define UBX_NAV_VELECEF 0x11
#define UBX_NAV_VELNED 0x12

#define UBX_RXM_ALM 0x30
#define UBX_RXM_EPH 0x31
#define UBX_RXM_PMREQ 0x41
#define UBX_RXM_RAW 0x10
#define UBX_RXM_SFRB 0x11
#define UBX_RXM_SVSI 0x20

#define UBX_TIM_TIM2 0x03
#define UBX_TIM_TP 0x01
#define UBX_TIM_VRFY 0x06

// Customized position report
struct posit {
  uint32_t iTOW;
  uint16_t year;
  uint8_t month;
  uint8_t day;
  uint8_t hour;
  uint8_t min;
  uint8_t sec;
  int32_t nano;
  uint8_t fixType;
  union {
    uint8_t flagsRaw;
    struct {
      uint8_t gnssFixOK : 1;
      uint8_t psmState : 3;
      uint8_t diffSoln: 1;
      uint8_t unused1 : 3;
    } flags;
  };
  int32_t lon;
  int32_t lat;
  int32_t hMSL;
  int32_t gSpeed;
  int32_t heading;
  uint16_t pDOP;
  uint32_t hAcc;
  uint32_t vAcc;
  uint8_t numSV;
} __attribute((packed))__;

// The NAV-PVT structure alone
struct pvt {
  uint32_t iTOW;
  uint16_t year;
  uint8_t  month;
  uint8_t  day;
  uint8_t  hour;
  uint8_t  min;
  uint8_t  sec;
  union {
    uint8_t  validRaw;
    struct {
      uint8_t unused1 : 5;
      uint8_t fullResolved : 1;
      uint8_t validTime : 1;
      uint8_t validDate : 1;
    } valid;
  };
  uint32_t tAcc;
  int32_t  nano;
  uint8_t  fixType;
  union {
    uint8_t  flagsRaw;
    struct {
      uint8_t gnssFixOK : 1;
      uint8_t psmState : 3;
      uint8_t diffSoln: 1;
      uint8_t unused1 : 3;
    } flags;
  };
  uint8_t  reserved1;
  uint8_t  numSV;
  int32_t  lon;
  int32_t  lat;
  int32_t  height;
  int32_t  hMSL;
  uint32_t hAcc;
  uint32_t vAcc;
  int32_t  velN;
  int32_t  velE;
  int32_t  velD;
  int32_t  gSpeed;
  int32_t  heading;
  uint32_t sAcc;
  uint32_t headingAcc;
  uint16_t pDOP;
  uint16_t reserved2;
  uint32_t reserved3;
};

struct ubx {
  uint8_t cls;
  uint8_t id;
  uint16_t len;
  uint8_t ck_a;
  uint8_t ck_b;
  union {
    union {
      struct ack {
        uint8_t clsID;
        uint8_t msgID;
      } ack;
      struct nak {
        uint8_t clsID;
        uint8_t msgID;
      } nak;
    } ack;
    union {
      struct {
        uint32_t iTOW;
        int32_t clkB;
        int32_t clkD;
        uint32_t tAcc;
        uint32_t fAcc;
      } clock;
      struct pvt pvt;
      struct {
        uint32_t iTOW;
        uint8_t gpsFix;
        union {
          uint8_t flagsRaw;
          struct {
            uint8_t unused1 : 4;
            uint8_t towSet : 1;
            uint8_t wknSet : 1;
            uint8_t diffSoln : 1;
            uint8_t gpsFixOk : 1;
          } flags;
        };
        union {
          uint8_t fixStatRaw;
          struct {
            uint8_t mapMatching : 2;
            uint8_t unused1 : 5;
            uint8_t dgpsIStat : 1;
          } fixStat;
        };
        union {
          uint8_t flags2Raw;
          struct {
            uint8_t unused1 : 6;
            uint8_t psmState : 2;
          } flags2;
        };
        uint32_t ttff;
        uint32_t msss;
      } status;
      struct {
        uint32_t iTOW;
        int32_t fTOW;
        int16_t week;
        int8_t leapS;
        union {
          uint8_t validRaw;
          struct {
            uint8_t unused1 : 5;
            uint8_t leapSValid : 1;
            uint8_t weekValid : 1;
            uint8_t towValid : 1;
          } valid;
        };
        uint32_t tAcc;
      } timegps;
      struct {
        uint32_t iTOW;
        uint32_t tAcc;
        int32_t nano;
        uint16_t year;
        uint8_t month;
        uint8_t day;
        uint8_t hour;
        uint8_t min;
        uint8_t sec;
        union {
          uint8_t validRaw;
          struct {
            uint8_t unused1 : 5;
            uint8_t validUTC : 1;
            uint8_t validWKN : 1;
            uint8_t validTOW : 1;
          } valid;
        };
      } timeutc;
      struct {
        uint32_t iTOW;
        int32_t ecefVX;
        int32_t ecefVY;
        int32_t ecefVZ;
        uint32_t sAcc;
      } velecef;
      struct {
        uint32_t iTOW;
        int32_t velN;
        int32_t velE;
        int32_t velD;
        uint32_t speed;
        uint32_t gSpeed;
        int32_t heading;
        uint32_t sAcc;
        uint32_t cAcc;
      } velned;
    } nav;
    union {
      struct {
        uint8_t payload[72];
      } hui;
      /*struct {
        union {
          uint32_t svid;
          uint8_t svid8;
        };
        union {
          uint8_t payloadRaw[36];
          struct {
            uint32_t week;
            uint32_t dwrd[8];
          } payload;
        };
      } alm;
      struct {
        union {
          uint32_t svid;
          uint8_t svid8;
        };
        uint32_t how;
        uint32_t sf1d[8], sf2d[8], sf3d[8];
      } eph;
      struct {
        uint8_t svid;
        union {
          uint8_t payloadRaw[203];
          struct {
            uint8_t data[59];
            uint8_t optional1[48];
            uint8_t optional2[48];
            uint8_t optional3[48];
          } payload;
        };
      } aop;*/
    } aid;
  };
} ubx;
